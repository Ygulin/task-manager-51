package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonFasterXmlLoadRequest extends AbstractUserRequest {

    public DataJsonFasterXmlLoadRequest(@Nullable final String token) {
        super(token);
    }

}
