package ru.tsc.gulin.tm.dto.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    @Nullable
    private String table;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private final Date date = new Date();


    @NotNull
    private Object entity;

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @NotNull
    private String type;

    public OperationEvent(
            @NotNull final Object entity,
            @NotNull final String type
    ) {
        this.entity = entity;
        this.type = type;
    }

}
