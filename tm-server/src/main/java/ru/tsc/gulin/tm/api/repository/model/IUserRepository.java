package ru.tsc.gulin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.User;
import ru.tsc.gulin.tm.enumerated.Role;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role
    );

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    Boolean isLoginExists(@NotNull String login);

    Boolean isEmailExists(@NotNull String email);

}
