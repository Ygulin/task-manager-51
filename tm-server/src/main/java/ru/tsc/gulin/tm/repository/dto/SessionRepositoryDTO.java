package ru.tsc.gulin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;
import ru.tsc.gulin.tm.dto.model.SessionDTO;
import ru.tsc.gulin.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class SessionRepositoryDTO extends AbstractUserOwnedRepositoryDTO<SessionDTO> implements ISessionRepositoryDTO {

    public SessionRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}
