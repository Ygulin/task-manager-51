package ru.tsc.gulin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.model.ISessionRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.model.ISessionService;
import ru.tsc.gulin.tm.model.Session;
import ru.tsc.gulin.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}
