package ru.tsc.gulin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.gulin.tm.dto.model.TaskDTO;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gulin.tm.exception.field.*;
import ru.tsc.gulin.tm.repository.dto.TaskRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.*;

public class TaskServiceDTO extends AbstractUserOwnedServiceDTO<TaskDTO, ITaskRepositoryDTO> implements ITaskServiceDTO {

    public TaskServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected TaskRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepositoryDTO(entityManager);
    }

    @Override
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateProjectIdById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
