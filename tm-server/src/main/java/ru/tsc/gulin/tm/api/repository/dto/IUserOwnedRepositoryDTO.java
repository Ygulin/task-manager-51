package ru.tsc.gulin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends IRepositoryDTO<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId, @Nullable Comparator comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull M model);

    void update(@NotNull String userId, @NotNull M model);

}
