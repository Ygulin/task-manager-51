package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.event.OperationEvent;

public interface ISenderService {

    void send(@NotNull String message);

}
